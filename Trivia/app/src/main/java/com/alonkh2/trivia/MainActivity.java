package com.alonkh2.trivia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RadioGroup answers;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        answers = findViewById(R.id.answers);
        submit = findViewById(R.id.submit);
    }

    public void handleSubmit(View view) {
        if (view == submit){
            if (answers.getCheckedRadioButtonId() == -1){
                Context context = getApplicationContext();
                CharSequence text = "Illegal input! Please try again.";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
            else {
                Intent i1 = new Intent(this, Question2.class);
                if (answers.getCheckedRadioButtonId() == R.id.a1) {
                    i1.putExtra("points", 1);
                }
                else{
                    i1.putExtra("points", 0);
                }
                startActivity(i1);
            }
        }
    }
}