package com.alonkh2.trivia;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Question2 extends AppCompatActivity {
    RadioGroup answers;
    TextView points;
    Button submit, restart;
    Intent i1;
    int point;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question2);

        answers = findViewById(R.id.answers);
        points = findViewById(R.id.points);
        submit = findViewById(R.id.submit);
        restart = findViewById(R.id.restart);

        i1 = this.getIntent();
        point = i1.getIntExtra("points", 0);

        points.setText("Points: " + point);
    }

    public void handleSubmit(View view) {
        if (view == submit){
            if (answers.getCheckedRadioButtonId() == -1){
                Context context = getApplicationContext();
                CharSequence text = "Illegal input! Please try again.";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
            else {
                Intent i1 = new Intent(this, Question3.class);
                if (answers.getCheckedRadioButtonId() == R.id.a3) {
                    i1.putExtra("points", point + 1);
                }
                else{
                    i1.putExtra("points", point);
                }
                startActivity(i1);
            }
        }
        if (view == restart){
            Intent i1 = new Intent(this, MainActivity.class);
            startActivity(i1);
        }
    }
}