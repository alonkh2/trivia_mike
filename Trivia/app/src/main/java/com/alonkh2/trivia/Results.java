package com.alonkh2.trivia;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Results extends AppCompatActivity {
    TextView points;
    Intent i1;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        points = findViewById(R.id.points);

        i1 = getIntent();

        int point = i1.getIntExtra("points", 0);

        points.setText("Total points: " + point);
    }

    public void handleSubmit(View view) {
        Intent i1 = new Intent(this, MainActivity.class);
        startActivity(i1);
    }
}